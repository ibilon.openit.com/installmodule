import rootdir

prefix    = 'installmodule'
channel   = 'release'
root      = rootdir.get()
etc       = root / 'etc'
tmp       = root / 'tmp'
var       = root / 'var' / prefix
logs      = root / 'var' / 'log'
metadata  = var  / 'metadata'
downloads = var  / 'downloads'
apifile   = etc  / 'api.txt'
api       = apifile.read_text().strip()
