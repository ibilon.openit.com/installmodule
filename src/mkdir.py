def mkdir(path):
    path.mkdir(parents=True, exist_ok=True)
    return path
