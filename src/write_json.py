import json

def write_json(dictionary, filepath):
    with open(filepath, 'w') as file:
        file.write(json.dumps(dictionary, indent=4))
    return dictionary
