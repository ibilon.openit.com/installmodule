from tempfile import TemporaryDirectory
import settings

def create_tempdir():
    return TemporaryDirectory(dir=settings.tmp, prefix=settings.prefix)
