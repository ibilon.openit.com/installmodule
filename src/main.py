from install import install
from pathlib import Path
from mkdir import mkdir
import entrypoint
import argparse
import settings
import logging
import log

def main():
    args    = get_args()
    modules = args.module
    configure(args)
    for module in modules:
        install(module)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('module', nargs='+')
    return parser.parse_args()

def configure(args):
    log.init(filename=settings.prefix, directory=settings.logs)
    logging.getLogger().setLevel(logging.INFO)
    mkdir(settings.tmp)
    mkdir(settings.var)
    mkdir(settings.metadata)
    mkdir(settings.downloads)

if __name__ == '__main__':
    entrypoint.call(main)
