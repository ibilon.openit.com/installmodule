from pathlib import Path
import sys

def get():
    filepath  = Path(sys.executable if getattr(sys, 'frozen', False) else __file__)
    directory = filepath.parent / '..'
    return directory.resolve()
