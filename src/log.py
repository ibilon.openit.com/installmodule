from logging.handlers import RotatingFileHandler
from logging import StreamHandler, Formatter
from sys import stderr
import logging
import os

max_bytes    = 1e+6
backup_count = 2
root         = logging.getLogger()

def init(filename=None, directory=None):
    if filename and directory:
        handler = create_rotating_file_handler(filename, directory)
    else:
        handler = create_stream_handler()
    root.addHandler(handler)

def create_rotating_file_handler(filename, directory):
    os.makedirs(directory, exist_ok=True)
    filename  = filename + '.log'
    filepath  = os.path.join(directory, filename)
    handler   = RotatingFileHandler(filepath, maxBytes=max_bytes, backupCount=backup_count, encoding='utf-8')
    formatter = Formatter(fmt='%(levelname)s %(asctime)s %(name)s %(message)s')
    handler.setFormatter(formatter)
    return handler

def create_stream_handler(stream=stderr):
    handler   = StreamHandler(stream)
    formatter = Formatter(fmt='%(message)s')
    handler.setFormatter(formatter)
    return handler
