from tqdm import tqdm

def write(source, destination, expected_size):
    blocksize  = 4194304
    total_size = 0
    with tqdm(total=expected_size, unit='B', unit_scale=True) as progress_bar:
        while True:
            buffer     = source.read(blocksize)
            size       = len(buffer)
            total_size = total_size + size
            progress   = int(total_size / expected_size * 100)
            if not buffer:
                break
            destination.write(buffer)
            progress_bar.update(size)
    assert total_size == expected_size
