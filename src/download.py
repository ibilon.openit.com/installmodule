from metadata import is_metadata_exists, get_metadata, delete_metadata, save_metadata
from create_tempdir import create_tempdir
from get_arch import get_arch
from get_time import get_time
from get_os import get_os
from pathlib import Path
from shutil import move
import progress_writer
import urllib.request
import settings

def download(module_name, channel):
    url = get_url(module_name, channel)
    with urllib.request.urlopen(url) as response:
        etag = get_etag(response)
        size = get_size(response)
        with create_tempdir() as directory:
            directory = Path(directory)
            tempfile  = directory / module_name
            filepath  = get_download_filepath(module_name)
            metadata  = get_metadata(module_name)
            metadata['etag']    = etag
            metadata['channel'] = channel
            metadata['size']    = dict(compressed=size)
            metadata['downloaded_at'] = get_time()
            with open(tempfile, 'wb') as file:
                progress_writer.write(response, file, size)
            move(tempfile, filepath)
            save_metadata(metadata)

def get_url(module_name, channel):
    os   = get_os()
    arch = get_arch()
    url  = f'{settings.api}/services/servemoduled/channels/{channel}/operating-systems/{os}/architectures/{arch}/modules/{module_name}'
    return url

def get_size(response):
    return int(response.headers['Content-Length'])

def get_etag(response):
    return response.headers['Etag']

def get_download_filepath(module_name):
    extension = '.tar.gz'
    filename  = module_name + extension
    filepath  = settings.downloads / filename
    return filepath

def is_downloaded(module_name, etag):
    filepath = get_download_filepath(module_name)
    if filepath.exists() and is_metadata_exists(module_name):
        metadata = get_metadata(module_name)
        if 'etag' in metadata:
            if etag == metadata['etag']:
                return True
    return False
