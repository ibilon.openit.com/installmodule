from metadata import get_metadata, save_metadata
from download import get_download_filepath
from create_tempdir import create_tempdir
from shutil import copyfileobj
from pathlib import Path
import settings
import tarfile
import gzip

def scan(module_name):
    with create_tempdir() as directory:
        directory = Path(directory)
        extension = '.tar'
        filename  = module_name + extension
        package   = directory / filename
        targz     = get_download_filepath(module_name)
        unpack(targz, package)
        with tarfile.open(package) as tar:
            members                          = tar.getmembers()
            contents                         = tar.getnames()
            dependencies                     = get_dependencies(tar, module_name)
            size                             = get_size(tar)
            metadata                         = get_metadata(module_name)
            metadata['contents']             = contents
            metadata['dependencies']         = dependencies
            metadata['size']['uncompressed'] = size
            return save_metadata(metadata)

def unpack(source, destination):
    with gzip.open(source, 'rb') as source:
        with open(destination, 'wb') as destination:
            copyfileobj(source, destination)

def get_dependency_file(module_name):
    return f'var/requirements/{module_name}.txt'

def get_dependencies(tar_object, module_name):
    try:
        newline      = '\n'
        filepath     = get_dependency_file(module_name)
        obj          = tar_object.extractfile(filepath)
        buffer       = obj.read()
        string       = buffer.decode()
        string       = string.strip()
        lines        = string.split(newline)
        dependencies = [ line.strip() for line in lines ]
        return dependencies
    except Exception:
        pass
    return []

def get_size(members):
    total_size = 0
    for member in members:
        size = member.size
        total_size += size
    return total_size
