import platform

def get_os():
    system  = platform.system()
    mapping = {'Windows': 'windows', 'Linux': 'linux', 'Darwin': 'darwin', 'SunOS': 'solaris'}
    assert system in mapping
    return mapping[system]
