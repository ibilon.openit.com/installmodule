from metadata import is_metadata_exists, get_metadata, save_metadata, create_metadata
from download import download, get_url, is_downloaded, get_download_filepath
from urllib.request import Request, urlopen
from urllib.error import HTTPError
from shutil import unpack_archive
from read_json import read_json
from get_time import get_time
from scanner import scan
import settings
import logging

logger = logging.getLogger(__name__)
stack  = []

def install(name):
    module_name, channel = parse(name)
    install_module(module_name, channel)

def parse(name):
    separator   = '@'
    tokens      = name.split(separator)
    module_name = tokens[0]
    channel     = settings.channel if len(tokens) < 2 else tokens[1]
    return module_name, channel

def install_module(module_name, channel):
    if not module_name in stack:
        stack.append(module_name)
        etag, final_channel = get_etag(module_name, channel)
        logger.info(f'Installing {module_name} from {final_channel}')
        if not is_installed(module_name, etag):
            metadata = create_metadata(module_name)
            if not is_downloaded(module_name, etag):
                logger.info('Downloading')
                download(module_name, final_channel)
            install_dependencies(module_name, channel)
            commit(module_name)
            logger.info(f'Successfully installed {module_name}')
        else:
            logger.info(f'Already installed')
            install_dependencies(module_name, channel)

def install_dependencies(module_name, channel):
    metadata = get_metadata(module_name)
    if not 'dependencies' in metadata:
        metadata = scan(module_name)
    dependencies = metadata['dependencies']
    if dependencies:
        logger.info(f'Dependencies {dependencies}')
        for dependency in dependencies:
            install_module(dependency, channel)

def get_etag(module_name, channel):
    etag    = None
    url     = get_url(module_name, channel)
    request = Request(url, method='HEAD')
    try:
        with urlopen(request) as response:
            etag = response.headers['Etag']
    except HTTPError as e:
        if e.code == 404:
            logger.info(f'{module_name} not found in {channel}')
            if channel != settings.channel:
                return get_etag(module_name, settings.channel)
        raise e
    return etag, channel

def is_installed(module_name, etag):
    if is_metadata_exists(module_name):
        metadata = get_metadata(module_name)
        if 'is_installed' in metadata and 'etag' in metadata:
            if metadata['is_installed'] == True and metadata['etag'] == etag:
                return True
    return False

def commit(module_name):
    unpack(module_name)
    register(module_name)

def unpack(module_name):
    filepath = get_download_filepath(module_name)
    unpack_archive(filepath, settings.root)

def register(module_name):
    metadata = get_metadata(module_name)
    metadata['is_installed'] = True
    metadata['installed_at'] = get_time()
    save_metadata(metadata)
