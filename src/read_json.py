import json

def read_json(filepath):
    with open(filepath) as file:
        return json.loads(file.read())
