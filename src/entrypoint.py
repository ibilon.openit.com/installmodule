import logging
import signal
import log
import sys

logger = logging.getLogger(__name__)

def init():
    signals()
    log.init()

def call(main):
    try:
        init()
        main()
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logger.exception(e)
        sys.exit(1)

def terminate(*args):
    sys.exit(2)

def signals():
    signal.signal(signal.SIGINT,  terminate)
    signal.signal(signal.SIGTERM, terminate)
