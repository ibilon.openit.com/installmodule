from write_json import write_json
from read_json import read_json
import settings

def get_metadata(module_name):
    filepath = get_metadata_filepath(module_name)
    return read_json(filepath)

def save_metadata(metadata):
    module_name = metadata['name']
    filepath    = get_metadata_filepath(module_name)
    return write_json(metadata, filepath)

def is_metadata_exists(module_name):
    filepath = get_metadata_filepath(module_name)
    return filepath.exists()

def delete_metadata(module_name):
    filepath = get_metadata_filepath(module_name)
    if filepath.exists():
        filepath.unlink()

def create_metadata(module_name):
    metadata = dict(name=module_name)
    return save_metadata(metadata)

def get_metadata_filepath(module_name):
    extension = '.json'
    filename  = module_name + extension
    filepath  = settings.metadata / filename
    return filepath
