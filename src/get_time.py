import datetime

def get_time():
    return datetime.datetime.now().astimezone().isoformat()
