import platform

def get_arch():
    machine = platform.machine()
    mapping = {'i386': '386', 'AMD64': 'amd64', 'x86_64': 'amd64', 'arm64': 'arm64'}
    assert machine in mapping
    return mapping[machine]
