@echo off
set root=%~dp0\..
cd  %root%
rmdir /s /q env
rmdir /s /q dist
rmdir /s /q build
del   /s /q *.spec
python -m venv env
call env\Scripts\activate.bat
python -m pip install wheel
python -m pip install PyInstaller
python -m pip install -r requirements.txt
python -m PyInstaller --version-file version --onefile src\main.py --name openit_installmodule --icon icon.ico --add-data C:\Windows\System32\vcruntime140.dll;.
